﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDAPI.BusinessEntities.General;
using SDAPI.BusinessEntities.Reputation;
using SDAPI.DataLayer;

namespace SDAPI.BusinessLogic
{
    public static class ReputationManager
    {
        public static ReviewStatsSummary GetReviewStatsSummary(StatsRequest statsRequest)
        {
            var results = new ReviewStatsSummary();

            try
            {
                results = Reputation.GetReviewStatsSummary(statsRequest);

            }
            catch (Exception)
            {

                throw;
            }
            
            return results;
        }

        public static ReviewStatsDetail GetReviewStatsDetail(StatsRequest statsRequest)
        {
            var results = new ReviewStatsDetail();

            try
            {
                results = Reputation.GetReviewStatsDetail(statsRequest);
            }
            catch (Exception)
            {

                throw;
            }
            
            return results;
        }
    }
}
