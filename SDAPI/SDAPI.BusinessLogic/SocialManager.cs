﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDAPI.BusinessEntities.General;
using SDAPI.BusinessEntities.Social;
using SDAPI.DataLayer;

namespace SDAPI.BusinessLogic
{
    public static class SocialManager
    {
        public static FBPageStats GetFacebookPageStats(StatsRequest statsRequest)
        {
            var results = new FBPageStats();

            try
            {
                results = Social.GetFacebookPageStats(statsRequest);
            }
            catch (Exception)
            {
                
                throw;
            }


            return results;
        }
    }
}
