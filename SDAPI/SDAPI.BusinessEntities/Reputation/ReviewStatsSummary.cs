﻿namespace SDAPI.BusinessEntities.Reputation
{
    public class ReviewStatsSummary : IReviewStatsSummary
    {
        public string LocationName { get; set; }
        public string ReviewSource { get; set; }
        public int ReviewCount { get; set; }
        public string ReviewSourceURL { get; set; }
        public decimal Rating { get; set; }
        public string RatingAdj { get; set; }
        public string RatingHTML { get; set; }
    }
}