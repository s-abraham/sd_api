﻿using SDAPI.BusinessEntities.General;

namespace SDAPI.BusinessEntities.Reputation
{
    public interface IReviewStatsSummary : IStats
    {
        string ReviewSource { get; set; }
        int ReviewCount { get; set; }
        string ReviewSourceURL { get; set; }
        decimal Rating { get; set; }
        string RatingAdj { get; set; }
        string RatingHTML { get; set; }
    }
}