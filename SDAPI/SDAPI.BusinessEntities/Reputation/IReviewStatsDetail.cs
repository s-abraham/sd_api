﻿using System;
using SDAPI.BusinessEntities.General;

namespace SDAPI.BusinessEntities.Reputation
{
    public interface IReviewStatsDetail : IStats
    {
        string ReviewSource { get; set; }
        DateTime ReviewDate { get; set; }
        decimal Rating { get; set; }
        string RatingAdj { get; set; }
        string RatingHTML { get; set; }
        string ReviewerName { get; set; }
        string ReviewText { get; set; }
    }
}