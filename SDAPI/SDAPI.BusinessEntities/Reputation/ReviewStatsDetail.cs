﻿using System;

namespace SDAPI.BusinessEntities.Reputation
{
    public class ReviewStatsDetail : IReviewStatsDetail
    {
        public string LocationName { get; set; }
        public string ReviewSource { get; set; }
        public DateTime ReviewDate { get; set; }
        public decimal Rating { get; set; }
        public string RatingAdj { get; set; }
        public string RatingHTML { get; set; }
        public string ReviewerName { get; set; }
        public string ReviewText { get; set; }
    }
}