﻿using System;

namespace SDAPI.BusinessEntities.General
{
    public interface IStatsRequest
    {
        DateTime QueryDate { get; set; }
        int Month { get; set; }
        int Week { get; set; }
        int Year { get; set; }
        Guid RequesterGuid { get; set; }
        char RequestPeriod { get; set; } //w = week, d = day, m = month
        int ExternalCustomerId { get; set; }
        int StatTypeId { get; set; } // 1 = Social, 2 = Reputation
    }
}