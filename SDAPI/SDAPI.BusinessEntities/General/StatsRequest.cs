﻿using System;

namespace SDAPI.BusinessEntities.General
{
    public class StatsRequest : IStatsRequest
    {
        public DateTime QueryDate { get; set; }
        public int Month { get; set; }
        public int Week { get; set; }
        public int Year { get; set; }
        public Guid RequesterGuid { get; set; }
        public char RequestPeriod { get; set; } //w = week, d = day, m = month
        public int ExternalCustomerId { get; set; }
        public int StatTypeId { get; set; } // 1 = Social, 2 = Reputation
    }
}