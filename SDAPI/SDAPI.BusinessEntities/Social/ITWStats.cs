﻿using SDAPI.BusinessEntities.General;

namespace SDAPI.BusinessEntities.Social
{
    public interface ITWStats : IStats
    {
        int TwitterId { get; set; }
        string TwitterIdStr { get; set; }
        string TwitterName { get; set; }
        string ScreenName { get; set; }
        string Location { get; set; }
        string Url { get; set; }
        int FollowersCount { get; set; }
        int FriendsCount { get; set; }
        int ListedCount { get; set; }
        string CreatedAt { get; set; }
        int FavouritesCount { get; set; }
        int? UtcOffset { get; set; }
        string TimeZone { get; set; }
        bool GeoEnabled { get; set; }
        bool Verified { get; set; }
        int StatusesCount { get; set; }
        string Lang { get; set; }
    }
}