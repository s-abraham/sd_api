﻿using SDAPI.BusinessEntities.General;

namespace SDAPI.BusinessEntities.Social
{
    public interface IFBPageInsights : IStats
    {
        string page_fan_adds_unique { get; set; }
        string page_fan_adds { get; set; }
        string page_fan_removes_unique { get; set; }
        string page_fan_removes { get; set; }
        string page_subscriber_adds_unique { get; set; }
        string page_subscriber_adds { get; set; }
        string page_subscriber_removes_unique { get; set; }
        string page_subscriber_removes { get; set; }
        
        string page_views { get; set; }
        
        string page_views_internal_referrals { get; set; }
        string page_views_external_referrals { get; set; }
        
        string page_story_adds_unique { get; set; }
        string page_story_adds { get; set; }
        
        string page_story_adds_by_story_type_unique { get; set; }
        string page_story_adds_by_story_type { get; set; }
        
        string page_impressions_by_age_gender_unique { get; set; }
        string page_impressions_by_country_unique { get; set; }
        string page_impressions_by_locale_unique { get; set; }
        string page_impressions_by_city_unique { get; set; }
        
        string page_story_adds_by_age_gender_unique { get; set; }
        string page_story_adds_by_country_unique { get; set; }
        string page_story_adds_by_city_unique { get; set; }
        string page_story_adds_by_locale_unique { get; set; }

        string page_impressions_unique { get; set; }
        string page_impressions { get; set; }
        string page_impressions_paid_unique { get; set; }
        string page_impressions_paid { get; set; }
        string page_impressions_organic_unique { get; set; }
        string page_impressions_organic { get; set; }
        
        string page_impressions_by_story_type_unique { get; set; }
        string page_impressions_by_story_type { get; set; }
        string page_places_checkin_total { get; set; }
        string page_places_checkin_total_unique { get; set; }
        string page_places_checkin_mobile { get; set; }
        string page_places_checkin_mobile_unique { get; set; }
        string page_places_checkins_by_age_gender { get; set; }
        string page_places_checkins_by_country { get; set; }
        string page_places_checkins_by_city { get; set; }
        string page_places_checkins_by_locale { get; set; }

        string page_posts_impressions_unique { get; set; }
        string page_posts_impressions { get; set; }
        string page_posts_impressions_paid_unique { get; set; }
        string page_posts_impressions_paid { get; set; }
        string page_posts_impressions_organic_unique { get; set; }
        string page_posts_impressions_organic { get; set; }
        string page_posts_impressions_viral_unique { get; set; }
        string page_posts_impressions_viral { get; set; }
        
        string page_fans_by_like_source_unique { get; set; }
        string page_fans_by_like_source { get; set; }
        
        string page_subscribers_by_subscribe_source_unique { get; set; }
        string page_subscribers_by_subscribe_source { get; set; }
        
        string page_negative_feedback_unique { get; set; }
        string page_negative_feedback { get; set; }
        string page_negative_feedback_by_type_unique { get; set; }
        string page_negative_feedback_by_type { get; set; }
        
        string page_fans { get; set; }
        string page_fans_locale { get; set; }
        string page_fans_city { get; set; }
        string page_fans_country { get; set; }
        string page_fans_gender { get; set; }
        string page_fans_age { get; set; }
        string page_fans_gender_age { get; set; }
        string page_friends_of_fans { get; set; }
        
        string page_subscribers { get; set; }
        string page_subscribers_locale { get; set; }
        string page_subscribers_city { get; set; }
        string page_subscribers_country { get; set; }
        string page_subscribers_gender_age { get; set; }
        
        string page_storytellers { get; set; }
        string page_storytellers_by_story_type { get; set; }
        string page_storytellers_by_age_gender { get; set; }
        string page_storytellers_by_country { get; set; }
        string page_storytellers_by_city { get; set; }
        string page_storytellers_by_locale { get; set; }
        
        string page_engaged_users { get; set; }
        
        string page_impressions_frequency_distribution { get; set; }
        string page_impressions_viral_frequency_distribution { get; set; }
        
        string page_posts_impressions_frequency_distribution { get; set; }
        string page_views_unique { get; set; }
        string page_stories { get; set; }
        string page_stories_by_story_type { get; set; }
    }
}