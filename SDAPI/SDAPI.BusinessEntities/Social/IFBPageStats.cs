﻿using SDAPI.BusinessEntities.General;

namespace SDAPI.BusinessEntities.Social
{
    public interface IFBPageStats : IStats
    {
        string PageName { get; set; }
        string Website { get; set; }
        string Username { get; set; }
        string Phone { get; set; }
        int Checkins { get; set; }
        int WereHereCount { get; set; }
        int TalkingAboutCount { get; set; }
        int UnreadNotifCount { get; set; }
        int NewLikeCount { get; set; }
        string PageId { get; set; }
        string PageLink { get; set; }
        int Likes { get; set; }
        int UnreadMessageCount { get; set; }
        int UnseenMessageCount { get; set; }
    }
}