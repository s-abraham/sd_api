﻿using System;

namespace SDAPI.BusinessEntities.Social
{
    public class FBPageStats : IFBPageStats
    {
        public string LocationName { get; set; }
        public string PageName { get; set; }
        public string Website { get; set; }
        public string Username { get; set; }
        public string Phone { get; set; }
        public int Checkins { get; set; }
        public int WereHereCount { get; set; }
        public int TalkingAboutCount { get; set; }
        public int UnreadNotifCount { get; set; }
        public int NewLikeCount { get; set; }
        public string PageId { get; set; }
        public string PageLink { get; set; }
        public int Likes { get; set; }
        public int UnreadMessageCount { get; set; }
        public int UnseenMessageCount { get; set; }
    }
}