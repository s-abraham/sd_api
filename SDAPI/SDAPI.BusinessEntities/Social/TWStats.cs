﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDAPI.BusinessEntities.Social
{
    public class TWStats : ITWStats
    {
        public string LocationName { get; set; }
        public int TwitterId { get; set; }
        public string TwitterIdStr { get; set; }
        public string TwitterName { get; set; }
        public string ScreenName { get; set; }
        public string Location { get; set; }
        public string Url { get; set; }
        public int FollowersCount { get; set; }
        public int FriendsCount { get; set; }
        public int ListedCount { get; set; }
        public string CreatedAt { get; set; }
        public int FavouritesCount { get; set; }
        public int? UtcOffset { get; set; }
        public string TimeZone { get; set; }
        public bool GeoEnabled { get; set; }
        public bool Verified { get; set; }
        public int StatusesCount { get; set; }
        public string Lang { get; set; }
    }
}
