﻿using SDAPI.BusinessEntities.General;

namespace SDAPI.BusinessEntities.Social
{
    public interface IGPStats: IStats   
    {
        int PlusOnersCount { get; set; }
        int ResharersCount { get; set; }
    }
}