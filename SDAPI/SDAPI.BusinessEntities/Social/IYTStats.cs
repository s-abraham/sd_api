﻿using SDAPI.BusinessEntities.General;

namespace SDAPI.BusinessEntities.Social
{
    public interface IYTStats: IStats   
    {
        int SubscriberCount { get; set; }
        int ChannelViewsCount { get; set; }
        int TotalUploadViews { get; set; }
        int VideosCount { get; set; }
        int SubscriptionsCount { get; set; }
        int FavoritesCount { get; set; }
        int ContactsCount { get; set; }
    }
}