﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDAPI.BusinessEntities.Social
{
    public class GpStats : IGPStats
    {
        public string LocationName { get; set; }
        public int PlusOnersCount { get; set; }
        public int ResharersCount { get; set; }
    }
}
