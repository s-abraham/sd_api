﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDAPI.BusinessEntities.Social
{
    public class YTStats : IYTStats
    {
        public string LocationName { get; set; }
        public int SubscriberCount { get; set; }
        public int ChannelViewsCount { get; set; }
        public int TotalUploadViews { get; set; }
        public int VideosCount { get; set; }
        public int SubscriptionsCount { get; set; }
        public int FavoritesCount { get; set; }
        public int ContactsCount { get; set; }
    }
}
