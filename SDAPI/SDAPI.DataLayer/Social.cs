﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDAPI.BusinessEntities.General;
using SDAPI.BusinessEntities.Social;

namespace SDAPI.DataLayer
{
    public static class Social
    {
        public static FBPageStats GetFacebookPageStats(StatsRequest statsRequest)
        {
            var results = new FBPageStats();

            try
            {
                //Replace with Data Access Code
                results.LocationName = "Castle Chevrolet";
                results.PageName = "Castle Chevy";
                results.Website = "http://test.com";
                results.Username = "Castle Chevrolet";
                results.Phone = "630-123-4567";
                results.Checkins = 4546;
                results.WereHereCount = 455;
                results.TalkingAboutCount = 654;
                results.UnreadNotifCount = 7;
                results.NewLikeCount = 3;
                results.PageId = "4554564564878464685";
                results.PageLink = "http://www.facebook.com/test";
                results.Likes = 456545;
                results.UnreadMessageCount = 4;
                results.UnseenMessageCount = 8;

            }
            catch (Exception)
            {

                throw;
            }


            return results;
        }

        public static FBPageInsights GetFacebookPageInsightsStats(StatsRequest statsRequest)
        {
            var results = new FBPageInsights();

            try
            {
                //Replace with Data Access Code
                results.LocationName = "Castle Chevrolet";
                results.page_fan_adds_unique  = "Test Results";
                results.page_fan_adds  = "Test Results";
                results.page_fan_removes_unique  = "Test Results";
                results.page_fan_removes  = "Test Results";
                results.page_subscriber_adds_unique  = "Test Results";
                results.page_subscriber_adds  = "Test Results";
                results.page_subscriber_removes_unique  = "Test Results";
                results.page_subscriber_removes  = "Test Results";
                results.page_views  = "Test Results";
                results.page_views_internal_referrals  = "Test Results";
                results.page_views_external_referrals  = "Test Results";
                results.page_story_adds_unique  = "Test Results";
                results.page_story_adds  = "Test Results";
                results.page_story_adds_by_story_type_unique  = "Test Results";
                results.page_story_adds_by_story_type  = "Test Results";
                results.page_impressions_by_age_gender_unique  = "Test Results";
                results.page_impressions_by_country_unique  = "Test Results";
                results.page_impressions_by_locale_unique  = "Test Results";
                results.page_impressions_by_city_unique  = "Test Results";
                results.page_story_adds_by_age_gender_unique  = "Test Results";
                results.page_story_adds_by_country_unique  = "Test Results";
                results.page_story_adds_by_city_unique  = "Test Results";
                results.page_story_adds_by_locale_unique  = "Test Results";
                results.page_impressions_unique  = "Test Results";
                results.page_impressions  = "Test Results";
                results.page_impressions_paid_unique  = "Test Results";
                results.page_impressions_paid  = "Test Results";
                results.page_impressions_organic_unique  = "Test Results";
                results.page_impressions_organic  = "Test Results";
                results.page_impressions_by_story_type_unique  = "Test Results";
                results.page_impressions_by_story_type  = "Test Results";
                results.page_places_checkin_total  = "Test Results";
                results.page_places_checkin_total_unique  = "Test Results";
                results.page_places_checkin_mobile  = "Test Results";
                results.page_places_checkin_mobile_unique  = "Test Results";
                results.page_places_checkins_by_age_gender  = "Test Results";
                results.page_places_checkins_by_country  = "Test Results";
                results.page_places_checkins_by_city  = "Test Results";
                results.page_places_checkins_by_locale  = "Test Results";
                results.page_posts_impressions_unique  = "Test Results";
                results.page_posts_impressions  = "Test Results";
                results.page_posts_impressions_paid_unique  = "Test Results";
                results.page_posts_impressions_paid  = "Test Results";
                results.page_posts_impressions_organic_unique  = "Test Results";
                results.page_posts_impressions_organic  = "Test Results";
                results.page_posts_impressions_viral_unique  = "Test Results";
                results.page_posts_impressions_viral  = "Test Results";
                results.page_fans_by_like_source_unique  = "Test Results";
                results.page_fans_by_like_source  = "Test Results";
                results.page_subscribers_by_subscribe_source_unique  = "Test Results";
                results.page_subscribers_by_subscribe_source  = "Test Results";
                results.page_negative_feedback_unique  = "Test Results";
                results.page_negative_feedback  = "Test Results";
                results.page_negative_feedback_by_type_unique  = "Test Results";
                results.page_negative_feedback_by_type  = "Test Results";
                results.page_fans  = "Test Results";
                results.page_fans_locale  = "Test Results";
                results.page_fans_city  = "Test Results";
                results.page_fans_country  = "Test Results";
                results.page_fans_gender  = "Test Results";
                results.page_fans_age  = "Test Results";
                results.page_fans_gender_age  = "Test Results";
                results.page_friends_of_fans  = "Test Results";
                results.page_subscribers  = "Test Results";
                results.page_subscribers_locale  = "Test Results";
                results.page_subscribers_city  = "Test Results";
                results.page_subscribers_country  = "Test Results";
                results.page_subscribers_gender_age  = "Test Results";
                results.page_storytellers  = "Test Results";
                results.page_storytellers_by_story_type  = "Test Results";
                results.page_storytellers_by_age_gender  = "Test Results";
                results.page_storytellers_by_country  = "Test Results";
                results.page_storytellers_by_city  = "Test Results";
                results.page_storytellers_by_locale  = "Test Results";
                results.page_engaged_users  = "Test Results";
                results.page_impressions_frequency_distribution  = "Test Results";
                results.page_impressions_viral_frequency_distribution  = "Test Results";
                results.page_posts_impressions_frequency_distribution  = "Test Results";
                results.page_views_unique  = "Test Results";
                results.page_stories  = "Test Results";
                results.page_stories_by_story_type  = "Test Results";

            }
            catch (Exception)
            {

                throw;
            }


            return results;
        }

        public static GpStats GetGooglePlusStats(StatsRequest statsRequest)
        {
            var results = new GpStats();

            try
            {
                //Replace with Data Access Code
                results.LocationName = "Castle Chevrolet";
                results.PlusOnersCount = 89;
                results.ResharersCount = 75;


            }
            catch (Exception)
            {

                throw;
            }


            return results;
        }

        public static TWStats GetTwitterStats(StatsRequest statsRequest)
        {
            var results = new TWStats();

            try
            {
                //Replace with Data Access Code
                results.LocationName = "Castle Chevrolet";
                results.TwitterId  = 1234;
                results.TwitterIdStr  = "132456";
                results.TwitterName  = "CastleChevrolet";
                results.ScreenName  = "CastleChevrolet";
                results.Location  = "Villa Park, IL";
                results.Url  = "http://www.twitter.com/castlechevrolet";
                results.FollowersCount  = 454;
                results.FriendsCount  = 65;
                results.ListedCount  = 5;
                results.CreatedAt  = DateTime.Today.AddDays(-365).ToString();
                results.FavouritesCount  = 785;
                results.UtcOffset  = -6;
                results.TimeZone  = "Central";
                results.GeoEnabled  = true;
                results.Verified  = true;
                results.StatusesCount  = 545;
                results.Lang  = "eng";
            }
            catch (Exception)
            {

                throw;
            }


            return results;
        }

        public static YTStats GetYouTubeStats(StatsRequest statsRequest)
        {
            var results = new YTStats();

            try
            {
                //Replace with Data Access Code
                results.LocationName = "Castle Chevrolet";
                results.SubscriberCount = 215;
                results.ChannelViewsCount = 151;
                results.TotalUploadViews = 54148;
                results.VideosCount = 384;
                results.SubscriptionsCount = 5645;
                results.FavoritesCount = 54;
                results.ContactsCount = 654;
            }
            catch (Exception)
            {

                throw;
            }


            return results;
        }
    }
}
