﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDAPI.BusinessEntities.General;
using SDAPI.BusinessEntities.Reputation;
using SDAPI.BusinessEntities.Social;

namespace SDAPI.DataLayer
{
    public static class Reputation
    {
        public static ReviewStatsSummary GetReviewStatsSummary(StatsRequest statsRequest)
        {
            var results = new ReviewStatsSummary();

            try
            {
                //Replace with Data Access Code
                results.LocationName = "Castle Chevrolet";
                results.ReviewSource = "Google+";
                results.ReviewCount = 543;
                results.ReviewSourceURL = "http://www.google.com/4545454";
                results.Rating = 4.2m;
                results.RatingAdj = "29";
                results.RatingHTML = "placeholder";

            }
            catch (Exception)
            {

                throw;
            }


            return results;
        }

        public static ReviewStatsDetail GetReviewStatsDetail(StatsRequest statsRequest)
        {
            var results = new ReviewStatsDetail();

            try
            {
                //Replace with Data Access Code
                results.LocationName = "Castle Chevrolet";
                results.ReviewSource = "Google+";
                results.ReviewDate = DateTime.Today;
                results.ReviewerName = "bob";
                results.ReviewText = "good service";
                results.Rating = 4.2m;
                results.RatingAdj = "29";
                results.RatingHTML = "placeholder";

            }
            catch (Exception)
            {

                throw;
            }


            return results;
        }

    }
}
