﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SDAPI.BusinessEntities.General;
using SDAPI.BusinessEntities.Reputation;
using SDAPI.BusinessLogic;

namespace SDAPI.WS.Controllers
{
    public class ReviewsController : ApiController
    {
        public int Get()
        {
            var results = 0;

            results = 234;

            return results;
        }

        public ReviewStatsSummary GetReviewStatsSummary(StatsRequest statsRequest)
        {
            var results = new ReviewStatsSummary();

            try
            {
                results = ReputationManager.GetReviewStatsSummary(statsRequest);

            }
            catch (Exception)
            {

                throw;
            }

            return results;
        }

        public ReviewStatsDetail GetReviewStatsDetail(StatsRequest statsRequest)
        {
            var results = new ReviewStatsDetail();

            try
            {
                results = ReputationManager.GetReviewStatsDetail(statsRequest);
            }
            catch (Exception)
            {

                throw;
            }

            return results;
        }
    }
}
